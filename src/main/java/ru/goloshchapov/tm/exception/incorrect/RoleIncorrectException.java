package ru.goloshchapov.tm.exception.incorrect;

import ru.goloshchapov.tm.exception.AbstractException;

public class RoleIncorrectException extends AbstractException {

    public RoleIncorrectException(final String message) {
        super("Error! Role " + message + " is incorrect...");
    }

}
