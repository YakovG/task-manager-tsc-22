package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class TaskNotCreatedException  extends AbstractException {

    public TaskNotCreatedException() {
        super("Error! Task not created...");
    }

}
