package ru.goloshchapov.tm.entity;

public interface IHaveName {

    String getName();

    void setName(String name);

}
