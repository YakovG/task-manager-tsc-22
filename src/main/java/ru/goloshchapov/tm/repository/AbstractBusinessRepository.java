package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.IBusinessRepository;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<M extends AbstractBusinessEntity>
        extends AbstractRepository<M> implements IBusinessRepository<M> {

    public Predicate<M> predicateByUserId(final String userId) {
        return m -> m.checkUserAccess(userId);
    }

    public Predicate<M> predicateById(final String userId, final String modelId) {
        return m -> m.checkUserAccess(userId) && modelId.equals(m.getId());
    }

    public Predicate<M> predicateByName(final String userId, final String name) {
        return m -> m.checkUserAccess(userId) && name.equals(m.getName());
    }

    public Predicate<M> predicateByStarted(final String userId) {
        return m -> m.checkUserAccess(userId) && (m.getStatus() != Status.NOT_STARTED);
    }

    public Predicate<M> predicateByCompleted(final String userId) {
        return m -> m.checkUserAccess(userId) && (m.getStatus() == Status.COMPLETE);
    }

    private M start (final M m) {
        m.setStatus(Status.IN_PROGRESS);
        m.setDateStart(new Date());
        return m;
    }

    private M finish (final M m) {
        m.setStatus(Status.COMPLETE);
        m.setDateFinish(new Date());
        return m;
    }

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        list.add(model);
        return model;
    }

    @Override
    public void addAll(final String userId, final Collection<M> collection) {
        if (collection == null) return;
        collection.forEach(c->c.setUserId(userId));
        list.addAll(collection);
    }

    @Override
    public List<M> findAll(final String userId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAllByUserId(final String userId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAllStarted(final String userId, final Comparator<M> comparator) {
        return list.stream()
                .filter(predicateByStarted(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAllCompleted(final String userId, final Comparator<M> comparator) {
        return list.stream()
                .filter(predicateByCompleted(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String userId, final String modelId) {
        return list.stream()
                .filter(predicateById(userId, modelId))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        final M model = list.get(index);
        if (model.checkUserAccess(userId)) return model;
        return null;
    }

    @Override
    public M findOneByName(final String name) {
        return list.stream()
                .filter(m -> name.equals(m.getName()))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByName(final String userId, final String name) {
        return list.stream()
                .filter(predicateByName(userId, name))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Override
    public boolean isAbsentByName(final String name) {
        return findOneByName(name) == null;
    }

    @Override
    public String getIdByName(final String name) {
        return findOneByName(name).getId();
    }

    @Override
    public int size(final String userId) {
        return findAll(userId).size();
    }

    @Override
    public void remove(final String userId, final M model) {
        if (model.checkUserAccess(userId)) list.remove(model);
    }

    @Override
    public void clear(final String userId) {
        list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList())
                .forEach(this::remove);
    }

    @Override
    public M removeOneById(final String userId, final String modelId) {
        final Optional<M> model = Optional.ofNullable(findOneById(userId,modelId));
        model.ifPresent(list::remove);
        return model.orElse(null);
    }

    @Override
    public M removeOneByIndex(final String userId, final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(list::remove);
        return model.orElse(null);
    }

    @Override
    public M removeOneByName(final String userId, final String name) {
        final Optional<M> model = Optional.ofNullable(findOneByName(userId, name));
        model.ifPresent(list::remove);
        return model.orElse(null);
    }

    @Override
    public M startOneById(final String userId, final String modelId) {
        final Optional<M> model = Optional.ofNullable(findOneById(userId, modelId));
        return model.map(this::start).orElse(null);
    }

    @Override
    public M startOneByIndex(final String userId, final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        return model.map(this::start).orElse(null);
    }

    @Override
    public M startOneByName(final String userId, final String name) {
        final Optional<M> model = Optional.ofNullable(findOneByName(userId, name));
        return model.map(this::start).orElse(null);
    }

    @Override
    public M finishOneById(final String userId, final String modelId) {
        final Optional<M> model = Optional.ofNullable(findOneById(userId, modelId));
        return model.map(this::finish).orElse(null);

    }

    @Override
    public M finishOneByIndex(final String userId, final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        return model.map(this::finish).orElse(null);
    }

    @Override
    public M finishOneByName(final String userId, final String name) {
        final Optional<M> model = Optional.ofNullable(findOneByName(userId, name));
        return model.map(this::finish).orElse(null);
    }

}
