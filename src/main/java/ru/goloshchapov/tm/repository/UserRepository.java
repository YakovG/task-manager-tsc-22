package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.model.User;

import java.util.Optional;
import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public Predicate<User> predicateByLogin (final String login) {
        return u-> login.equals(u.getLogin());
    }

    public Predicate<User> predicateByEmail (final String email) {
        return u -> email.equals(u.getEmail());
    }

    @Override
    public User findUserByLogin(final String login) {
        return list.stream()
                .filter(predicateByLogin(login))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Override
    public User findUserByEmail(final String email) {
        return list.stream().
                filter(predicateByEmail(email)).
                limit(1).findFirst()
                .orElse(null);
    }

    @Override
    public final boolean isLoginExists(final String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public final boolean isEmailExists(final String email) {
        return findUserByEmail(email) != null;
    }

    @Override
    public User removeUser(final User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final Optional<User> user = Optional.ofNullable(findUserByLogin(login));
        user.ifPresent(list::remove);
        return user.orElse(null);
    }

    @Override
    public User removeUserByEmail(final String email) {
        final Optional<User> user = Optional.ofNullable(findUserByEmail(email));
        user.ifPresent(list::remove);
        return user.orElse(null);
    }

    @Override
    public User lockUserByLogin(final String login) {
        final Optional<User> user = Optional.ofNullable(findUserByLogin(login));
        user.ifPresent(u-> u.setLocked(true));
        return user.orElse(null);
    }

    @Override
    public User unlockUserByLogin(final String login) {
        final Optional<User> user = Optional.ofNullable(findUserByLogin(login));
        user.ifPresent(u-> u.setLocked(false));
        return user.orElse(null);
    }

    @Override
    public User lockUserByEmail(final String email) {
        final Optional<User> user = Optional.ofNullable(findUserByEmail(email));
        user.ifPresent(u-> u.setLocked(true));
        return user.orElse(null);
    }

    @Override
    public User unlockUserByEmail(final String email) {
        final Optional<User> user = Optional.ofNullable(findUserByEmail(email));
        user.ifPresent(u-> u.setLocked(false));
        return user.orElse(null);
    }

}
