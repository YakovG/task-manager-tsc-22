package ru.goloshchapov.tm.model;

import ru.goloshchapov.tm.entity.IWBS;
import ru.goloshchapov.tm.enumerated.Status;

import java.util.Date;

public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    private String userId;

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date dateStart;

    private Date dateFinish;

    private Date created = new Date();

    public AbstractBusinessEntity() {
    }

    public AbstractBusinessEntity(final String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public Date getDateStart() {
        return dateStart;
    }

    @Override
    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    @Override
    public Date getDateFinish() {
        return dateFinish;
    }

    @Override
    public void setDateFinish(final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(final Date created) {
        this.created = created;
    }

    public boolean checkUserAccess(final String userId) {
        return userId.equals(getUserId());
    }

    @Override
    public String toString() { return getId() + ". " + name; }

}
