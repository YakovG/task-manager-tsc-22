package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.Project;

import java.util.List;

public final class ProjectAllViewCommand extends AbstractCommand {

    public static final String NAME = "project-view-all";

    public static final String DESCRIPTION = "Show all projects";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ALL PROJECTS]");
        final List<Project> projects = serviceLocator.getProjectService().findAll();
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
