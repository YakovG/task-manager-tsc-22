package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByIdStartCommand extends AbstractProjectCommand{

    public static final String NAME = "project-start-by-id";

    public static final String DESCRIPTION ="Start project by id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
    }
}
