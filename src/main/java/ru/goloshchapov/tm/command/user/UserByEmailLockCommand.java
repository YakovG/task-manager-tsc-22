package ru.goloshchapov.tm.command.user;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class UserByEmailLockCommand extends AbstractCommand {

    public static final String NAME = "user-lock-by-email";

    public static final String DESCRIPTION = "Lock user by email";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER EMAIL]");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByEmail(email);
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
