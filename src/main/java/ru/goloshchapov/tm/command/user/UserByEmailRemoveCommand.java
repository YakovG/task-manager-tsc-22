package ru.goloshchapov.tm.command.user;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class UserByEmailRemoveCommand extends AbstractCommand {

    public static final String NAME = "user-remove-by-email";

    public static final String DESCRIPTION = "Remove user by email";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE USER]");
        System.out.println("[ENTER EMAIL]");
        final String email = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().removeUserByEmail(email);
        final boolean result = serviceLocator.getProjectTaskService().removeAllByUserId(user.getId());
        if (!result) System.out.println("User with email " + email + "has had an EMPTY project list");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
