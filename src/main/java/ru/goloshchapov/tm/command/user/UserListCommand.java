package ru.goloshchapov.tm.command.user;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class UserListCommand extends AbstractCommand {

    public static final String NAME = "user-list";

    public static final String DESCRIPTION = "Show user list";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST]");
        final List<User> users = serviceLocator.getUserService().findAll();
        for (final User user:users) {
            if (user.isLocked()) System.out.print("LOCKED! ");
            System.out.print(user.getLogin()+ " ");
            if (!isEmpty(user.getEmail())) System.out.print(user.getEmail() + " ");
            System.out.println(user.getRole().getDisplayName());
        }
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
