package ru.goloshchapov.tm.command.user;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class UserByEmailUnlockCommand extends AbstractCommand {

    public static final String NAME = "user-unlock-by-email";

    public static final String DESCRIPTION = "Unlock user by email";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNLOCK USER]");
        System.out.println("[ENTER EMAIL]");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByEmail(email);
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
