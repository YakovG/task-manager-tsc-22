package ru.goloshchapov.tm.command.user;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.AbstractBusinessEntity;
import ru.goloshchapov.tm.model.User;

import java.util.List;

public final class UserAndProjectListCommand extends AbstractCommand {

    public static final String NAME = "user-project-list";

    public static final String DESCRIPTION = "Show all users with project-list";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USERS WITH PROJECT LIST]");
        final List<User> users = serviceLocator.getUserService().findAll();
        for (final User user:users) {
            System.out.println(user.getLogin() + "   " + user.getRole().getDisplayName());
            serviceLocator.getProjectService().findAll(user.getId())
                    .forEach(p -> System.out.println(p.toString()));
            System.out.println("--------------------------------");
        }
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
