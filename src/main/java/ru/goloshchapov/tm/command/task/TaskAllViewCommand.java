package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public final class TaskAllViewCommand extends AbstractCommand {

    public static final String NAME = "task-view-all";

    public static final String DESCRIPTION = "Show all tasks";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ALL TASKS]");
        final List<Task> tasks = serviceLocator.getTaskService().findAll();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
